<?php
require('conf.php');
include('header.html');
session_start();
if(!isset($_SESSION['user'])) {
	header('Location:index.html');
}
?>

<html>
<title>
	A Novel Inconsistency Management Tool in Distributed Firewall Network
</title>
<link rel="stylesheet" href="css/style.css" type="text/css" />
 <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js?ver=1.3.2'></script> 
    <script type="text/javascript">
        $(function() {
            var offset = $("#content-left").offset();
            var topPadding = 2;
            $(window).scroll(function() {
                if ($(window).scrollTop() > offset.top) {
                    $("#content-left").stop().animate({
                        marginTop: $(window).scrollTop() - offset.top + topPadding
                    });
                } else {
                    $("#content-left").stop().animate({
                        marginTop: 0
                    });
                };
            });
        });
    </script>
	<script type = "text/javascript">
    
</script>
</script>
<div id="separator"></div>
<div id="content-bg">
	<div id="content-container-up">
		<div id="content-container-middle">
			<div id="content-left">
				<img src="img/search.png" style="padding-left:20px;">
				<hr />
				<form action="" method="post">
				<input type="text" placeholder="Search eBooks" name="search" id="search">&emsp; 
				<input type="image" src="img/search-logo.png" width="20px"></a>
				</form>
				<br>
				<?php
					//left navigation
				?>
				<button value="submit" type="submit">
					<span>Filter Results</span>
				</button>
				</form>
			</div>
			<div class="content-middle">
				<center><img src="img/add_firewall.png" height=100px></center>
				<hr />
				
				<form action="" method="post">
				No of Firewalls: <input type="text" name="addfirewall">
				<input type="submit" value="ADD" style="margin-left:20px;">
				</form>
				<?php
					if(isset($_POST['addfirewall'])) { ?>
						
					<form action="" method="post">
					<div id="wrapper">
						<div id="header1">Add Firewalls</div>
						<div id="content">
							<div id='content-1'>Name</div>
							<div id='content-2'>IP address</div>
							<div id='content-3'>Is Root?</div>
													
					
					<?php
					for($i=1;$i<=$_POST['addfirewall'];$i++) {
						//echo "<tr><td><input type='text' name='name[]'></td>";
						echo "<div id='content-1'><input type='text' name='name[".$i."]' style='margin-left:-5px;width:120px;height:30px;padding:0px;margin:0px;'></div>";
						echo "<div id='content-2'><input type='text' name='ip[".$i."]' style='margin-left:-5px;width:180px;height:30px;padding:0px;margin:0px;'></div>";
						echo "<div id='content-3' style='height:32px;'><input type='radio' name='root' value='".$i."' ></div>";
					}
					?>
						
						</div>
						<div id="bottom"></div>
					</div><br><br>
					<center><button value="save" type="submit">
					<span>Add Firewalls</span>
				</button></center>
					</form>
				<?php 
					} 
				?>
				<?php
				if(isset($_POST['name'])) {
					$nameList = $_POST['name'];
					$i=1;$root=0;
					foreach ($nameList as $key => $value) {
						//echo $_POST['name'][$key] . "  ======  " . $_POST['ip'][$key] . "<br/>";

						if(isset($_POST['root']))
							if($i++==$_POST['root']) $root=1; else $root=0;
						$sql="INSERT INTO ip SET name='".$_POST['name'][$key]."', ip='".$_POST['ip'][$key]."', root=$root, user_ip=1";
						mysqli_query($dbc,$sql);
					} ?>
					<script>alert("Firewalls added successfully in database");</script>
				<?php
					}
				?>
				
				
		</div>
		<div id="content-container-down"></div>
	</div>
	<div id="separator"></div>
	<h3>A Project by Ameya Hanamsagar, Bhagyashree Borate, Ninad Jane & Aditi Wasvand<br>
 	Under the guidance of Prof. Santosh Darade</h3>
</div>
</body>
	
<?php
	if(isset($_GET['python'])) {
		//echo "in python";
		exec("LD_LIBRARY_PATH='' python python/cisco.py 2>&1", $output, $return);
		if($return) { ?>
			<script>alert('Done');</script>
		<?php
		}
		
	}
?>
	

