<?php
	require('conf.php');
	include('header.html');
?>

<html>
<body>
<div id="separator"></div>
<?php $adjacents = 5; 
$tbl_name="programs";
$num=0;
$query = "SELECT COUNT(*) as num FROM $tbl_name";
$total_pages = mysqli_fetch_array(mysqli_query($dbc,$query));
$total_pages = $total_pages[num];
$targetpage = "index.php"; 	//your file name  (the name of this file)
	$limit = 10; 								//how many items to show per page
	$page = $_GET['page'];
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
	
	/* Get data. */
	$sql = "SELECT * FROM $tbl_name ORDER BY program_id DESC LIMIT $start, $limit";
	$result = mysqli_query($dbc,$sql);
	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\"><ul>";
		//previous button
		if ($page > 1) 
			$pagination.= "<li><a href=\"$targetpage?page=$prev\"><span>Prev</span></a></li>";
		else
			$pagination.= "<li></li>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<li><span class=\"current\">$counter</span></li>";
				else
					$pagination.= "<li><a href=\"$targetpage?page=$counter\"><span>$counter</span></a></li>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li><span class=\"current\">$counter</span></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?page=$counter\"><span>$counter</span></a></li>";					
				}
				$pagination.= "<li>...</li>";
				$pagination.= "<li><a href=\"$targetpage?page=$lpm1\"><span>$lpm1</span></a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=$lastpage\"><span>$lastpage</span></a></li>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<li><a href=\"$targetpage?page=1\"><span>1</span></a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=2\"><span>2</span></a></li>";
				$pagination.= "<li>...</li>";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li><span class=\"current\">$counter</span></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?page=$counter\"><span>$counter</span></a></li>";					
				}
				$pagination.= "<li>...</li>";
				$pagination.= "<li><a href=\"$targetpage?page=$lpm1\"><span>$lpm1</span></a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=$lastpage\"><span>$lastpage</span></a></li>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<li><a href=\"$targetpage?page=1\"><span>1</span></a></li>";
				$pagination.= "<li><a href=\"$targetpage?page=2\"><span>2</span></a></li>";
				$pagination.= "<li>...</li>";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<li><span class=\"current\">$counter</span></li>";
					else
						$pagination.= "<li><a href=\"$targetpage?page=$counter\"><span>$counter</span></a></li>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<li><a href=\"$targetpage?page=$next\"><span>Next</span></a></li>";
		else
			$pagination.= "<li></li>";
		$pagination.= "</div></ul>\n";		
	}
?>
<html>
<head>
<link rel="stylesheet" href="css/style.css" type="text/css" />
 <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js?ver=1.3.2'></script> 
    <script type="text/javascript">
        $(function() {
            var offset = $("#content-left").offset();
            var topPadding = 15;
            $(window).scroll(function() {
                if ($(window).scrollTop() > offset.top) {
                    $("#content-left").stop().animate({
                        marginTop: $(window).scrollTop() - offset.top + topPadding
                    });
                } else {
                    $("#content-left").stop().animate({
                        marginTop: 0
                    });
                };
            });
        });
    </script>
	<script type = "text/javascript">
    
</script>
</head>
<body>

<div id="content-bg">
	<div id="content-container-up">
		<div id="content-container-middle">
			<div id="content-left">
				<img src="img/search.png" style="padding-left:20px;">
				<hr />
				<form action="" method="post">
				<input type="text" placeholder="Search Programs" name="search" id="search">&emsp; 
				<input type="image" src="img/search-logo.png" width="20px"></a>
				</form>
				<br>
				<?php
					$query1="SELECT * FROM lab ORDER by name ASC";
					$result1=mysqli_query($dbc,$query1) or die(mysqli_error($dbc)); ?>
					<form action="index.php" method="post">
					
				<?php while($row1=mysqli_fetch_array($result1))
					{
						$category=$row1['name']; ?>
						<input id="<?php echo $category; ?>" type="checkbox" name="category[]" value="<?php echo $category; ?>"><label for="<?php echo $category; ?>"> <p><?php echo $category; ?></label></p>
						<?php
					}
				?>
				<button value="submit" type="submit">
					<span>Filter Results</span>
				</button>
				</form>
			</div>
			<div class="content-middle">
				<center><img src="img/programs.png" height=70px></center>
				<hr />
				<?php
					if(isset($_POST['category']))
					{
						$i=0;
						foreach($_POST['category'] as $value)
						{
							$query="SELECT lab_id FROM lab WHERE name='$value'";
							$row=mysqli_fetch_array(mysqli_query($dbc,$query));
							$category_sel[$i]=$row['lab_id'];
							$i++;
						}
		
						include('filter_search.php');
					}
					elseif(isset($_POST['search'])) 
					{
						$query=$_POST['search'];
						include('search.php');
					}
				else include('programs.php'); ?>
				
		</div>
		<div id="content-container-down"></div>
	</div>
	<div id="separator"></div>
	<h3>Developed by Ameya Hanamsagar. Hosted by <a href="http://www.sudosaints.com">SudoSaints</a><br> Dedicated to PB</h3>
</div>
</body>