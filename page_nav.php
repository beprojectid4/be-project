<!-- include style -->
<link rel="stylesheet" type="text/css" href="css/style.css" />
<?php
//open database
include 'conf.php';
//include our awesome pagination
//class (library)
include 'libs/ps_pagination.php';

//query all data anyway you want
$query="SELECT * FROM books ORDER by book_id DESC";

//now, where gonna use our pagination class
//this is a significant part of our pagination
//i will explain the PS_Pagination parameters
//$conn is a variable from our config_open_db.php
//$sql is our sql statement above
//3 is the number of records retrieved per page
//4 is the number of page numbers rendered below
//null - i used null since in dont have any other
//parameters to pass (i.e. param1=valu1&param2=value2)
//you can use this if you're gonna use this class for search
//results since you will have to pass search keywords
$pager = new PS_Pagination( $dbc, $query, 3, 4, null );

//our pagination class will render new
//recordset (search results now are limited
//for pagination)

$rs = $pager->paginate(); 
$num = $rs->num_rows;
//get retrieved rows to check if
//there are retrieved data
?>
<h2><u>
				Recent eBooks:
				</u></h2><br> 
				
				<ol type="1">
				<?php
					
					
					while($row = $rs->fetch_assoc())
					{
						?>
													
								<div class="content-middle-books">
							
								<li>
								<?php echo $row['name'];  ?> <br>
								<div class="book-image">
															<img src="<?php echo $row['img']; ?>" class="border center">
														</div>
														<div class="book-des">
															Description: <?php echo $row['des']; ?>
															<br><u>Category:</u> <?php 
															$query="SELECT name FROM category WHERE category_id=".$row['category_id'];
															$row_category=mysqli_fetch_array(mysqli_query($dbc,$query));
															echo $row_category['name']; ?>
														</div>
														<div class="book-download">
															<a href="<?php echo $row_books['download']; ?>"><img src="img/download.png" style="height:64px;width:64px;padding-left:30px;"></a>
														</div>
														</li><br><br><br><br><br><br><br><br>
														<hr />
												</div>
										<?php
										} ?> 
						</ol>
							
<?php
//page-nav class to control
//the appearance of our page 
//number navigation
echo "<div class='page-nav'>";
echo "<ul>";
	//display our page number navigation
	echo $pager->renderFullNav();
echo "</ul>";
echo "</div>";

?>